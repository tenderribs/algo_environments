#include <boost/program_options.hpp>

#include "CPM.h"
#include "Image.h"
#include "OpticFlowIO.h"
#include "RIC.h"
#include "opencv2/ximgproc.hpp"  // for struct edge detection

namespace po = boost::program_options;

/* read semi-dense matches, stored as x1 y1 x2 y2 per line (other values on the same line is not taking into account) */
void ReadMatches(const char* filename, FImage& outMat) {
    float* tmp = new float[4 * 100000];  // max number of match pair
    FILE* fid = fopen(filename, "r");
    int nmatch = 0;
    float x1, x2, y1, y2;
    while (!feof(fid) && fscanf(fid, "%f %f %f %f%*[^\n]", &x1, &y1, &x2, &y2) == 4) {
        tmp[4 * nmatch] = x1;
        tmp[4 * nmatch + 1] = y1;
        tmp[4 * nmatch + 2] = x2;
        tmp[4 * nmatch + 3] = y2;
        nmatch++;
    }
    outMat.allocate(4, nmatch);
    memcpy(outMat.pData, tmp, nmatch * 4 * sizeof(float));
    fclose(fid);
    delete[] tmp;
}

// prepare cost map from Structured Edge Detector
void GetCostMap(char* imgName, FImage& outCostMap) {
    cv::Mat cvImg1 = cv::imread(imgName);
    int w = cvImg1.cols;
    int h = cvImg1.rows;
    outCostMap.allocate(w, h, 1);

    cv::Mat fImg1;
    // convert source image to [0-1] range
    cvImg1.convertTo(fImg1, cv::DataType<float>::type, 1 / 255.0);
    int borderSize = 10;
    cv::copyMakeBorder(fImg1, fImg1, borderSize, borderSize, borderSize, borderSize, cv::BORDER_REPLICATE);
    cv::Mat edges(fImg1.size(), fImg1.type());

    std::string src_dir = ".";
#ifdef __FILE__
    src_dir = __FILE__;
    size_t pos = src_dir.find_last_of('/');
    if (pos != std::string::npos) {
        src_dir = src_dir.substr(0, pos) + "/win32/";
    }
#endif  // __FILE__
    cv::Ptr<cv::ximgproc::StructuredEdgeDetection> sEdge = cv::ximgproc::createStructuredEdgeDetection(src_dir + "/model.yml.gz");
    sEdge->detectEdges(fImg1, edges);
    // save result to FImage
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            outCostMap[i * w + j] = edges.at<float>(i + borderSize, j + borderSize);
        }
    }
}

void ReadEdges(char* fileName, FImage& edge) {
    int w = edge.width();
    int h = edge.height();
    FILE* fp = fopen(fileName, "rb");
    int size = fread(edge.pData, sizeof(float), w * h, fp);
    assert(size == w * h);
    fclose(fp);
}

void save_results_to_disk(const char* filename, int w, int h, const FImage& u, const FImage& v) {
    // save output flow
    char baseName[256], outName[256];

    // get base Name, not support multi-char language, like CJK
    strcpy(baseName, filename);
    char* dot = strrchr(baseName, '.');
    if (dot != NULL) dot[0] = '\0';

    // save the flow and the visualization image
    strcpy(outName, baseName);
    strcat(outName, ".ric.flo");
    OpticFlowIO::WriteFlowFile(u.pData, v.pData, w, h, outName);
    strcpy(outName, baseName);
    strcat(outName, ".ric.png");
    OpticFlowIO::SaveFlowAsImage(outName, u.pData, v.pData, w, h);
}

int exec_ric(const std::string& in1, const std::string& in2, const std::string& out, const std::string& flo) {
    FImage img1, img2;
    FImage matches, costMap;

    img1.imread(in1.c_str());
    GetCostMap((char*)in1.c_str(), costMap);
    img2.imread(in2.c_str());

    // 	costMap.allocate(img1.width(), img1.height(), 1);
    // 	ReadEdges(argv[3], costMap);

    // -------
    // Instead of reading matches from text file, we can just invoke CPM directly here
    // ReadMatches(argv[3], matches); <-- previously
    CPM c = CPM();
    c.Matching(img1, img2, matches);
    // ------------

    int w = img1.width();
    int h = img1.height();
    if (img2.width() != w || img2.height() != h) {
        printf("RIC can only handle images with the same dimension!\n");
        return -1;
    }

    RIC ric;
    FImage u, v;
    ric.SetSuperpixelSize(100);
    ric.Interpolate(img1, img2, costMap, matches, u, v);

    if (!out.empty()) {
        save_results_to_disk(out.c_str(), w, h, u, v);
    }

    if (!flo.empty()) {
        std::cout << "calculating EPE for: " << flo << std::endl;
        FImage u_gt = FImage(w, h, 2);
        FImage v_gt = FImage(w, h, 2);
        int gtw, gth;

        OpticFlowIO::ReadFlowFile(u_gt.pData, v_gt.pData, &gtw, &gth, flo.c_str());
        FlowErr err = OpticFlowIO::CalcFlowError(u.pData, v.pData, u_gt.pData, v_gt.pData, w, h);
        std::cout << flo << "\tEPE:\t" << err.aee << std::endl;
    }

    return 0;
}

int main(int argc, char** argv) {
    std::string in1, in2, out, flo;

    po::options_description desc("Allowed options");
    desc.add_options()("in1", po::value<std::string>(&in1)->required(), "Input file 1 (required)")("in2", po::value<std::string>(&in2)->required(), "Input file 2 (required)")("out", po::value<std::string>(&out), "Output file (optional)")("flo", po::value<std::string>(&flo), "Flow file (optional)")("help,h", "Display this help message");

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv)
                  .options(desc)
                  .style(po::command_line_style::default_style |
                         po::command_line_style::allow_long_disguise)
                  .run(),
              vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 1;  // Optionally, you can exit after showing help.
    }

    try {
        po::notify(vm);
    } catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }

    return exec_ric(in1, in2, out, flo);
}
