import numpy as np

# Read the floats from the text file
with open('epe_output_raw.txt', 'r') as file:
    lines = file.read().splitlines()
    floats = [float(line) for line in lines]

# Print the results
print(f"Mean: {np.mean(floats):.5f}")
print(f"Median: {np.median(floats):.5f}")
print(f"Maximum: {np.max(floats):.5f}")
print(f"Minimum: {np.min(floats):.5f}")
