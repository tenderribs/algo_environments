import subprocess, re, os, psutil
import numpy as np

from glob import glob
import os.path as osp


class FlowDataset:
    def __init__(self, name: str):
        self.name: str = name
        self.image_list = []
        self.flow_list = []

    def __getitem__(self, index):
        return (
            self.image_list[index][0],
            self.image_list[index][1],
            self.flow_list[index],
            self.name,
        )

    def __iter__(self):
        for index in range(len(self)):
            yield (
                self.image_list[index][0],
                self.image_list[index][1],
                self.flow_list[index],
            )

    def __len__(self):
        assert len(self.image_list) == len(self.flow_list), "Mismatched list lengths"
        return len(self.image_list)


class MpiSintel(FlowDataset):
    def __init__(self):
        super(MpiSintel, self).__init__("sintel")

        flow_root = "./datasets/Sintel/training/flow"
        image_root = "./datasets/Sintel/training/final"

        for scene in os.listdir(image_root):
            image_list = sorted(glob(osp.join(image_root, scene, "*.png")))
            for i in range(len(image_list) - 1):
                self.image_list += [[image_list[i], image_list[i + 1]]]

            self.flow_list += sorted(glob(osp.join(flow_root, scene, "*.flo")))

        print(f"found {len(self.image_list)} image pairs")
        print(f"found {len(self.flow_list)} flow files")

def run_ric_cpm(in1: str, in2: str, flo: str):
    ric_cmd: str = f"./RIC_CPM --in1 {in1} --in2 {in2} --flo {flo}"

    try:
        # Memory tracking
        memory_usage = []

        proc = subprocess.Popen(ric_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        process = psutil.Process(proc.pid)

        # Initialize memory usage tracking
        # rss: aka “Resident Set Size”, this is the non-swapped physical memory a process has used
        memory_usage = [process.memory_info().rss]

        while proc.poll() is None:
            # Monitor memory usage while the process is running
            memory_usage.append(process.memory_info().rss)

        # Decode standard output and standard error
        stdout, stderr = proc.communicate()
        output = stdout.decode()
        error_output = stderr.decode()

        if proc.returncode != 0:
            print(f"Error running RIC_CPM: {error_output}")
            return -1, -1

        epe_match = re.search(r"EPE:\s*([\d.]+)", output)

        if epe_match:
            epe = float(epe_match.group(1))
        else:
            print("EPE value not found in the RIC_CPM output.")
            epe = -1

        if epe > 0:
            # Calculate memory usage statistics
            return epe, max(memory_usage)

    except subprocess.CalledProcessError as e:
            print(f"Error running RIC_CPM: {e}")
    except Exception as ex:
        print(f"Error: {ex}")

    return -1, -1

def bench():
    try:
        dataset = MpiSintel()
        epes = []
        max_memory_usages = []

        if len(dataset) == 0:
            print("No images in dataset found. Check symlinks")
            return

        for in1, in2, flo in dataset:
            epe, max_memory = run_ric_cpm(in1, in2, flo)

            if epe > 0 and max_memory > 0:
                epes.append(epe)
                max_memory_usages.append(max_memory)

            if len(epes) % 10 == 0 and len(epes) != 0:
                print(len(epes), "/", len(dataset))
                print("Max Mem. Usage (MB): ", max(max_memory_usages) / (1024 * 1024))
                print("Av. EPE: ", np.mean(np.array(epes)), "\r\n")

        print("\r\n\r\nTotal Av. EPE: ", np.mean(np.array(epes)))
        print("Max Memory Usage (MB): ", max(max_memory_usages) / (1024 * 1024))

    except KeyboardInterrupt:
        with open("out_epes.txt", "w") as file:
            for item in epes:
                file.write(str(item) + "\n")
if __name__ == "__main__":
    bench()
