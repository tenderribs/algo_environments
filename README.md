# Algorithm Environments

Open the folder in VsCode Devcontainer on the server or locally.

IMPORTANT: If you want to use the GPU's functions, make sure to create a copy of the `.env.example` file renamed to `.env`. This `.env` file contains a flag that causes the Dockerfile to install the GPU related pip packages.

Then in the shell you still need to build RIC_CPM from source.
```bash
# enter folder
cd /workspaces/algo_environments/RIC_CPM/

# build the program
rm -rf build && mkdir build && cd build && cmake .. && make
```

## CPM + RIC

### Usage

```bash
# in /workspaces/algo_environments/RIC_CPM/
python3 bench.py
```

## PWCNet + LiteFlowNet



## Datasets

The datasets are already downloaded to the drive mounted as /datasets. Make sure to create symlinks so that the benchmarking scripts can find the files.

```bash
mkdir datasets

# For RIC_CPM
ln -s /datasets/mmarolf/MPI_Sintel_complete/ /home/mmarolf/algo_environments/RIC_CPM/datasets/Sintel
ln -s /datasets/mmarolf/KITTI2015/ /home/mmarolf/algo_environments/RIC_CPM/datasets/KITTI

# Same for RAFT
ln -s /datasets/mmarolf/MPI_Sintel_complete/ /home/mmarolf/algo_environments/RAFT/datasets/Sintel
ln -s /datasets/mmarolf/KITTI2015/ /home/mmarolf/algo_environments/RAFT/datasets/KITTI
```