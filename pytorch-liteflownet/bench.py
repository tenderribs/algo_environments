import sys
sys.path.append('core')

import torch, os, numpy, PIL, datasets

from run import Network, estimate
from utils import frame_utils
from utils.utils import InputPadder

@torch.no_grad()
def create_sintel_submission(save_to_disk=False, output_path='sintel_submission'):
    for dstype in ['clean', 'final']:
        print("\r\n\r\n::::::::::::::: Running MPI Sintel " + dstype + " :::::::::::::::")

        # load test split for both clean and final MPI Sintel runs
        test_dataset = datasets.MpiSintel(split='test', aug_params=None, dstype=dstype)
        scene: str = ""

        processed: int = 0
        for test_id in range(len(test_dataset)):

            # image1.shape is torch.Size([3, 436, 1024])
            # sequence is name of specific scene (string) and frame is frame_id (int)
            image1, image2, (sequence, frame) = test_dataset[test_id]

            # output progress to STDOUT
            if scene != sequence:
                scene = sequence
                print("%s\tprocessed:\t%d/%d" % (scene, processed, len(test_dataset)))

            if save_to_disk:
                flow = estimate(image1, image2)

                output_dir = os.path.join(output_path, dstype, sequence)
                output_file = os.path.join(output_dir, 'frame%04d.flo' % (frame+1))

                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)

                # estimate is in format (height, width, data)
                # .flo spec requires format (width, height, data)
                frame_utils.writeFlow(output_file, flow.permute(1, 2, 0).cpu().numpy())
            else:
                estimate(image1, image2)
            return

            processed += 1

if __name__ == '__main__':
    create_sintel_submission(save_to_disk=False)